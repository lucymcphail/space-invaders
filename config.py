class Config(object):
    def __init__(self):
        self.width = 1280
        self.height = 720
        self.fps = 60
        self.INVADER_ROWS = 4
        self.INVADER_COLUMNS = 10
