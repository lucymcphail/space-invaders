#!/usr/bin/env python
"""A simple space invaders clone written with pygame"""

import random

import pygame
import pygame.mixer

import bomb
import config
import invader
import missile
import player


class Game(object):
    def __init__(self):
        """Initialize pygame"""

        pygame.init()
        pygame.display.set_caption("Space Invaders")
        self.clock = pygame.time.Clock()
        self.playtime = 0.0
        self.font = pygame.font.Font("./taiko_font.ttf", 20)

        c = config.Config()
        self.width = c.width
        self.height = c.height
        self.fps = c.fps
        self.INVADER_ROWS = c.INVADER_ROWS
        self.INVADER_COLUMNS = c.INVADER_COLUMNS

        self.screen = pygame.display.set_mode(
            (self.width, self.height),
            pygame.DOUBLEBUF,
        )

        self.bg_txr = pygame.image.load("./img/background.jpg").convert()
        self.player_txr = pygame.image.load("./img/player.png").convert_alpha()
        self.invader_txrs = [
            pygame.image.load("./img/don_berry.png").convert_alpha(),
            pygame.image.load("./img/don_explorer.png").convert_alpha(),
            pygame.image.load("./img/don_fish.png").convert_alpha(),
            pygame.image.load("./img/don_frog.png").convert_alpha(),
            pygame.image.load("./img/don_girl.png").convert_alpha(),
            pygame.image.load("./img/don_hat.png").convert_alpha(),
            pygame.image.load("./img/don_headband.png").convert_alpha(),
            pygame.image.load("./img/don_pudding.png").convert_alpha(),
            pygame.image.load("./img/don_wings.png").convert_alpha(),
        ]
        self.missile_txr = pygame.image.load(
            "./img/missile.png").convert_alpha()
        self.bomb_txr = pygame.image.load("./img/bomb.png").convert_alpha()

        pygame.mixer.init()

        pygame.mixer.music.load("./aud/evangelion.ogg")
        pygame.mixer.music.play()

    def setup_player(self):
        self.player = player.Player(
            self.player_txr,
            self.screen.get_width() / 2 - self.player_txr.get_width() / 2,
            self.screen.get_height() - self.player_txr.get_height() - 40,
        )

    def setup(self):
        self.lives = 3

        self.score = 0
        self.can_fire_time = 0
        self.can_fire = True
        self.can_fire_delay = 1

        self.move_row = self.INVADER_ROWS - 1
        self.move_delay = 0.4
        self.can_move_time = self.move_delay
        # 1 = right, -1 = left
        self.move_dir = 1

        self.invader_fire_time = 2
        self.invader_fire_delay = 1.5

        self.setup_player()

        self.invader_rows = []

        random.shuffle(self.invader_txrs)

        width = max(
            *self.invader_txrs, key=lambda t: t.get_width()).get_width()
        height = max(
            *self.invader_txrs, key=lambda t: t.get_height()).get_height()

        for row in range(self.INVADER_ROWS):
            invader_row = []
            for col in range(self.INVADER_COLUMNS):
                txr = self.invader_txrs[row % len(self.invader_txrs)]

                invader_row.append(
                    invader.Invader(
                        txr,  # texture
                        (self.INVADER_ROWS - row) * 2,  # score
                        col * (width + 15) +
                        (self.screen.get_width() -
                         (self.INVADER_COLUMNS * width)) / 2,  # x
                        row * (height + 15) + 80,  # y
                        width,
                        height,
                    ))

            self.invader_rows.append(invader_row)

        self.missiles = []
        self.bombs = []

    def move_invaders(self, direction):
        for i in self.invader_rows[self.move_row]:
            i.rect.y += i.rect.height / 2

        if self.move_row == 0:
            self.move_row = self.INVADER_ROWS - 1
            self.move_dir = direction
        else:
            self.move_row -= 1

    def update_invaders(self, dt):
        for row in self.invader_rows:
            for i in row:
                i.update(self.screen.get_width(), dt)

                # remove inactive invaders
                if not i.is_active:
                    row.remove(i)

        # move invader rows
        if self.playtime > self.can_move_time:
            left_invader = self.invader_rows[0][0]
            right_invader = self.invader_rows[0][-1]

            left_x = left_invader.rect.x
            right_x = right_invader.rect.x

            left_min = left_invader.rect.width
            right_max = right_invader.rect.height * 2

            width = self.screen.get_width()

            if self.move_dir == -1 and left_x < left_min:
                self.move_invaders(1)
            elif self.move_dir == 1 and right_x > width - right_max:
                self.move_invaders(-1)
            else:
                # move invaders sideways
                for i in self.invader_rows[self.move_row]:
                    i.rect.x += i.rect.width / 2 * self.move_dir

                if self.move_row == 0:
                    self.move_row = len(self.invader_rows) - 1
                else:
                    self.move_row -= 1

            self.can_move_time = self.playtime + self.move_delay

    def update_missiles(self, dt):
        for m in self.missiles:
            m.update(dt)

            # remove inactive missiles
            if not m.is_active:
                self.missiles.remove(m)

            # check for invader collisions
            for row in self.invader_rows:
                for i in row:
                    off_x = i.rect.x - m.rect.x
                    off_y = i.rect.y - m.rect.y

                    if m.mask.overlap(i.mask, (off_x, off_y)):
                        i.is_active = False
                        m.is_active = False
                        self.score += i.score
                        break

            # check for bomb collisions
            for b in self.bombs:
                off_x = b.rect.x - m.rect.x
                off_y = b.rect.y - m.rect.y

                if m.mask.overlap(b.mask, (off_x, off_y)):
                    b.is_active = False
                    m.is_active = False
                    break

    def update_bombs(self, dt):
        for b in self.bombs:
            b.update(self.screen.get_height(), dt)

            if not b.is_active:
                self.bombs.remove(b)
                break

            # check for player collision
            off_x = self.player.rect.x - b.rect.x
            off_y = self.player.rect.y - b.rect.y

            if b.mask.overlap(self.player.mask, (off_x, off_y)):
                b.is_active = False
                self.setup_player()
                self.lives -= 1

        # drop a bomb above the player
        if self.playtime > self.invader_fire_time:
            # find invader above player
            player_rect = self.player.rect
            hit = False
            for row in self.invader_rows[::-1]:
                for i in row:
                    px = player_rect.centerx
                    il = i.rect.left
                    ir = i.rect.right

                    if px > il and px < ir:
                        self.bombs.append(
                            bomb.Bomb(self.bomb_txr, i.rect.x, i.rect.bottom))
                        hit = True
                if hit:
                    break

            self.invader_fire_time = self.playtime + self.invader_fire_delay

    def update(self, dt):
        # if there are no more invaders, restart
        for row in self.invader_rows:
            if row != []:
                break
            self.setup()

        self.update_invaders(dt)

        # update player
        self.player.update(self.screen.get_width(), dt)

        self.update_missiles(dt)

        # fire a missile
        if pygame.key.get_pressed()[pygame.K_SPACE]:
            if self.playtime > self.can_fire_time:
                self.missiles.append(
                    missile.Missile(
                        self.missile_txr,
                        self.player.rect.centerx -
                        self.missile_txr.get_width() / 2,
                        self.player.rect.top -
                        self.missile_txr.get_height() / 2,
                    ))

                self.player.can_fire = False
                self.can_fire_time = self.playtime + self.can_fire_delay

        self.update_bombs(dt)

        # check if the player is dead
        if self.lives <= 0:
            self.setup()

    def draw(self):
        self.player.draw(self.screen)

        for row in self.invader_rows:
            for i in row:
                i.draw(self.screen)

        for m in self.missiles:
            m.draw(self.screen)

        for b in self.bombs:
            b.draw(self.screen)

        self.draw_score(self.score)
        self.draw_lives(self.lives)

    def run(self):
        """The main loop
        """

        self.setup()

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False

            dt = self.clock.tick(self.fps)
            self.playtime += dt / 1000.0

            pygame.display.flip()
            self.screen.blit(self.bg_txr, (0, 0))

            self.update(dt)
            self.draw()

        pygame.quit()

    def draw_score(self, score):
        """Draw score in top left corner"""

        surface = self.font.render("Score: {}".format(score), True,
                                   (255, 255, 255))
        self.screen.blit(surface, (20, 20))

    def draw_lives(self, lives):
        """Draw lives in top right corner"""
        surface = self.font.render("Lives: {}".format(lives), True,
                                   (255, 255, 255))
        self.screen.blit(
            surface, (self.screen.get_width() - surface.get_width() - 20, 20))


if __name__ == "__main__":
    game = Game()
    game.run()
