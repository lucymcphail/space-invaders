import pygame
import pygame.mask


class Player:
    def __init__(self, txr, x, y):
        self.txr = txr
        self.rect = pygame.rect.Rect(x, y, txr.get_width(), txr.get_height())

        self.spd = 0.4
        self.can_fire = True

        self.mask = pygame.mask.from_surface(self.txr)

    def update(self, screenwidth, dt):
        if pygame.key.get_pressed()[pygame.K_LEFT]:
            self.rect.x -= int(self.spd * dt)

        if pygame.key.get_pressed()[pygame.K_RIGHT]:
            self.rect.x += int(self.spd * dt)

        if self.rect.right > screenwidth:
            self.rect.right = screenwidth
        if self.rect.left < 0:
            self.rect.left = 0

    def draw(self, screen):
        screen.blit(self.txr, self.rect)
