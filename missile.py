import pygame
import pygame.mask


class Missile:
    def __init__(self, txr, x, y):
        self.txr = txr
        self.rect = pygame.rect.Rect(x, y, txr.get_width(), txr.get_height())

        self.is_active = True
        self.spd = 0.6

        self.mask = pygame.mask.from_surface(self.txr)

    def update(self, dt):
        self.rect.y -= self.spd * dt

        if self.rect.bottom < 0:
            self.is_active = False

    def draw(self, screen):
        screen.blit(self.txr, self.rect)
