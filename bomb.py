import pygame
import pygame.mask


class Bomb:
    def __init__(self, txr, x, y):
        self.txr = txr
        self.rect = pygame.rect.Rect(x, y, txr.get_width(), txr.get_height())

        self.is_active = True
        self.spd = 0.25

        self.mask = pygame.mask.from_surface(self.txr)

    def update(self, screenheight, dt):
        self.rect.y += self.spd * dt

        if self.rect.y > screenheight:
            self.is_active = False

    def draw(self, screen):
        screen.blit(self.txr, self.rect)
