import pygame
import pygame.mask


class Invader:
    def __init__(self, txr, score, x, y, width, height):
        self.txr = txr
        self.rect = pygame.rect.Rect(x, y, width, height)
        self.score = score

        self.is_active = True

        self.mask = pygame.mask.from_surface(self.txr)

    def update(self, screenwidth, dt):
        pass

    def draw(self, screen):
        screen.blit(self.txr, self.rect)
